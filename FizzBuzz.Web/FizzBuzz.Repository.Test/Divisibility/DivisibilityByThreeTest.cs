﻿using FizzBuzz.Repository.Divisibility;
using FizzBuzz.Repository.Interface;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Test.Divisibility
{
    [TestFixture]
    public class DivisibilityByThreeTest
    {
        private Mock<ICheckForDay> mockDayCheck;
        private DivisibleByThree divisibilityByThree;

        [SetUp]
        public void LoadContext()
        {
            mockDayCheck = new Mock<ICheckForDay>();
            divisibilityByThree = new DivisibleByThree(mockDayCheck.Object);
        }

        [TestCase(15, true)]
        public void Check_IsNumber_Divisible_Should_Return_True_When_The_Input_Number_Is_Divisible_By_Three(int number, bool expected)
        {
            var actual = divisibilityByThree.IsDivisible(number);
            Assert.IsInstanceOf<bool>(actual, "Please enter a valid number");
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Fizz")]
        public void GetMessage_Should_Return_Fizz_When_DayOfWeek_IsNotWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsWednesday(It.IsAny<DayOfWeek>())).Returns(false);
            var actual = divisibilityByThree.GetMessage();
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Wizz")]
        public void GetMessage_Should_Return_Wizz_When_DayOfWeek_IsWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsWednesday(It.IsAny<DayOfWeek>())).Returns(true);
            var actual = divisibilityByThree.GetMessage();
            Assert.AreEqual(expected, actual);
        }

        [TearDown]
        public void TearDown_Test()
        {
            mockDayCheck = null;
            divisibilityByThree = null;
        }
    }
}
