﻿using FizzBuzz.Repository.Divisibility;
using FizzBuzz.Repository.Interface;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Test.Divisibility
{
    [TestFixture]
    public class DivisibilityByFiveTest
    {
        private Mock<ICheckForDay> mockDayCheck;
        private DivisibleByFive divisibilityByFive;

        [SetUp]
        public void LoadContext()
        {
            mockDayCheck = new Mock<ICheckForDay>();
            divisibilityByFive = new DivisibleByFive(mockDayCheck.Object);
        }

        [TestCase(5, true)]
        public void Check_IsNumber_Divisible_Should_Return_True_When_The_Input_Number_Is_Divisible_By_Five(int number, bool expected)
        {
            var actual = divisibilityByFive.IsDivisible(number);
            Assert.IsInstanceOf<bool>(actual, "Please enter a valid number");
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Buzz")]
        public void GetMessage_Should_Return_Buzz_When_DayOfWeek_IsNotWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsWednesday(It.IsAny<DayOfWeek>())).Returns(false);
            var actual = divisibilityByFive.GetMessage();
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Wuzz")]
        public void GetMessage_Should_Return_Wuzz_When_DayOfWeek_IsWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsWednesday(It.IsAny<DayOfWeek>())).Returns(true);
            var actual = divisibilityByFive.GetMessage();
            Assert.AreEqual(expected, actual);
        }

        [TearDown]
        public void TearDown_Test()
        {
            mockDayCheck = null;
            divisibilityByFive = null;
        }
    }
}
