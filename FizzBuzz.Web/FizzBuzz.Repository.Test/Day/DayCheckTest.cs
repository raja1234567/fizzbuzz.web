﻿using FizzBuzz.Repository.Interface;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Test.Day
{
    [TestFixture]
    public class DayCheckTest
    {
        private Mock<ICheckForDay> mockDayCheck;

        [SetUp]
        public void LoadContext()
        {
            mockDayCheck = new Mock<ICheckForDay>();
        }

        [TestCase(DayOfWeek.Wednesday, true)]
        [TestCase(DayOfWeek.Monday, false)]
        public void DayCheck_IsMatch_Should_Return_True_When_Input_Day_Is_Wednesday_Otherwise_False(DayOfWeek dayOfWeek, bool isMatch)
        {
            mockDayCheck.Setup(x => x.IsWednesday(dayOfWeek)).Returns(isMatch);
            var actual = mockDayCheck.Object.IsWednesday(dayOfWeek);
            Assert.AreEqual(isMatch, actual);
        }

        [TearDown]
        public void TearDown_Test()
        {
            mockDayCheck = null;
        }
    }
}
