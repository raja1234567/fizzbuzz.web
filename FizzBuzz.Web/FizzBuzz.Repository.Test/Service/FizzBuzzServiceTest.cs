﻿using FizzBuzz.Repository.Interface;
using FizzBuzz.Repository.Service;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Test.Service
{
    [TestFixture]
    public class FizzBuzzServiceTest
    {
        private FizzBuzzService fizzBuzzService;
        private Mock<IDivisible> divisibilityByThree;
        private Mock<IDivisible> divisibilityByFive;
        private IList<IDivisible> divisibilityRules;

        [SetUp]
        public void LoadContext()
        {
            divisibilityByFive = new Mock<IDivisible>();
            divisibilityByThree = new Mock<IDivisible>();
            divisibilityRules = new List<IDivisible>() { divisibilityByFive.Object, divisibilityByThree.Object };
            fizzBuzzService = new FizzBuzzService(divisibilityRules);
        }

        [TestCase(7)]
        public void GetFizzBuzzList_Should_Return_ListOfString_When_The_Input_Number_IsValid(int number)
        {
            var actual = fizzBuzzService.GetList(number);
            Assert.Greater(number, 0, "Please eneter a number");
            Assert.IsInstanceOf<List<string>>(actual);
        }

        [Test]
        public void GetFizzBuzzList_Should_Return_ListOfString_Contains_ListOfThree_Items_When_The_NumberIsThree()
        {
            divisibilityByThree.Setup(x => x.IsDivisible(3)).Returns(true);
            divisibilityByThree.Setup(x => x.GetMessage()).Returns("Fizz");
            var result = fizzBuzzService.GetList(3);
            Assert.AreEqual(result[0], "1");
            Assert.AreEqual(result[1], "2");
            Assert.AreEqual(result[2], "Fizz");
            divisibilityByThree.Verify(x => x.IsDivisible(It.IsAny<int>()), Times.Exactly(3));
        }

        [Test]
        public void GetFizzBuzzList_Should_Return_ListOfString_Contains_ListOfFive_Items_When_The_NumberIsFive()
        {
            divisibilityByThree.Setup(x => x.IsDivisible(3)).Returns(true);
            divisibilityByThree.Setup(x => x.GetMessage()).Returns("Fizz");
            divisibilityByFive.Setup(x => x.IsDivisible(5)).Returns(true);
            divisibilityByFive.Setup(x => x.GetMessage()).Returns("Buzz");
            var result = fizzBuzzService.GetList(5);
            Assert.AreEqual(result[0], "1");
            Assert.AreEqual(result[1], "2");
            Assert.AreEqual(result[2], "Fizz");
            Assert.AreEqual(result[3], "4");
            Assert.AreEqual(result[4], "Buzz");
            divisibilityByThree.Verify(x => x.IsDivisible(It.IsAny<int>()), Times.Exactly(5));
        }

        [TearDown]
        public void TearDown_Test()
        {
            divisibilityByFive = null;
            divisibilityByThree = null;
            divisibilityRules = null;
            fizzBuzzService = null;
        }
    }
}
