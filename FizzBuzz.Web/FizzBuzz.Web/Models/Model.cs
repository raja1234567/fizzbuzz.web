﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class Model
    {
        [Display(Name = "Enter Number")]
        [Required(ErrorMessage = "Number is required.")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Number must be a natural number")]
        public int? Number { get; set; }
        public IPagedList<string> FizzBuzzList { get; set; }
    }
}