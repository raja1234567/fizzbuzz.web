﻿using FizzBuzz.Web.Models;
using FizzBuzz.Repository.Interface;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzz.Web.Controllers
{
    public class FizzBuzzController : Controller
    {
        // GET: FizzBuzz
        private readonly IFizzBuzzService fizzBuzzService;
        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        [HttpGet]
        public ActionResult FizzBuzzTest()
        {
            return View("FizzBuzzTest");
        }

        public ActionResult Search(Model model, int? page)
        {
            if (ModelState.IsValid)
            {
                int pageNumber = page != null ? Convert.ToInt32(page) : 1;
                int pageSize = 10;
                var fizzBuzzList = this.fizzBuzzService.GetList(Convert.ToInt32(model.Number));
                if (fizzBuzzList != null)
                {
                    model.FizzBuzzList = fizzBuzzList.ToPagedList(pageNumber, pageSize);
                }
            }
            return View("FizzBuzzTest", model);
        }

    }
}