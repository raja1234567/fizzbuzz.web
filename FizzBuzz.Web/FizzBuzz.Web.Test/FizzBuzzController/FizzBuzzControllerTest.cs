﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzz.Repository.Interface;
using Moq;
using FizzBuzz.Web.Models;
using System.Web.Mvc;

namespace FizzBuzz.Web.Test.FizzBuzzController
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private Controllers.FizzBuzzController fizzBuzzController;
        private Mock<IFizzBuzzService> mockFizzBuzzService;
        private Model fizzBuzzModel;
        private List<string> fizzBuzzList;

        [SetUp]
        public void LoadContext()
        {
            mockFizzBuzzService = new Mock<IFizzBuzzService>();
            fizzBuzzController = new Controllers.FizzBuzzController(mockFizzBuzzService.Object);
            fizzBuzzList = new List<string>() { "1", "2", "Fizz", "4", "Buzz" };
        }

        [Test]
        public void FizzBuzzController_Get_FizzBuzzTest_Action_Should_Return_FizzBuzzTest_View_Page()
        {
            var actual = fizzBuzzController.FizzBuzzTest() as ViewResult;
            Assert.AreEqual(actual.ViewName, "FizzBuzzTest");
        }

        [TestCase(3)]
        [TestCase(5)]
        public void FizzBuzzController_GetFizzBuzzList_Action_Should_Return_View_With_Model_When_The_Input_Number_IsValid(int number)
        {
            int page = 1;
            fizzBuzzModel = new Model() { Number = number };
            mockFizzBuzzService.Setup(x => x.GetList(number)).Returns(fizzBuzzList);
            var result = fizzBuzzController.Search(fizzBuzzModel, page) as ViewResult;
            Assert.AreEqual(result.Model, fizzBuzzModel);
            Assert.AreEqual(result.ViewName, "FizzBuzzTest");
            mockFizzBuzzService.Verify(x => x.GetList(number), Times.Exactly(1));
        }

        [TestCase(0)]
        [TestCase(1001)]
        public void FizzBuzzController_GetFizzBuzzList_Action_Should_Return_ModelStateValidationMessage_When_The_Input_Number_Is_InValid(int number)
        {
            int page = 1;
            fizzBuzzModel = new Model() { Number = number };
            fizzBuzzController.ModelState.AddModelError("Number", "Please enter a number");
            mockFizzBuzzService.Setup(x => x.GetList(number)).Returns(fizzBuzzList);
            var result = fizzBuzzController.Search(fizzBuzzModel, page) as ViewResult;
            Assert.AreEqual(result.Model, fizzBuzzModel);
            Assert.AreEqual(result.ViewName, "FizzBuzzTest");
            mockFizzBuzzService.Verify(x => x.GetList(number), Times.Never);
        }

        [TearDown]
        public void TearDown_Test()
        {

        }
    }
}
