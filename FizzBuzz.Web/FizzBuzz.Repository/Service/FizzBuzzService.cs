﻿
using FizzBuzz.Repository.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Service
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IList<IDivisible> rules;
        public FizzBuzzService(IList<IDivisible> rules)
        {
            this.rules = rules;
        }

        public IList<string> GetList(int number)
        {
            var List = new List<string>();

            for (int index = 1; index <= number; index++)
            {
                var result = "";
                var matchlist = this.rules.Where(p => p.IsDivisible(index));
                if (matchlist.Any())
                {
                    var values = matchlist.Select(p => p.GetMessage());
                    result = string.Join("", values);
                }
                else
                {
                    result = index.ToString();
                }
                List.Add(result);
            }
            return List;
        }
    }
}
