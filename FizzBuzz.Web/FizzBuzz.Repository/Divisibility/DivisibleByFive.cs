﻿using FizzBuzz.Repository.Interface;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Divisibility
{
    public class DivisibleByFive : IDivisible
    {
        private readonly ICheckForDay dayToday;
        public DivisibleByFive(ICheckForDay dayToday)
        {
            this.dayToday = dayToday;
        }

        public bool IsDivisible(int Number)
        {
            return Number % 5 == 0;
        }
        public string GetMessage()
        {
            return (this.dayToday.IsWednesday(DateTime.Today.DayOfWeek)) == true ? "Wuzz" : "Buzz";
        }
    }
}
