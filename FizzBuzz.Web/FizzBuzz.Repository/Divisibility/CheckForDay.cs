﻿
using FizzBuzz.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Divisibility
{
    public class CheckForDay : ICheckForDay
    {
        public bool IsWednesday(DayOfWeek daytoday)
        {
            return daytoday == DayOfWeek.Wednesday;
        }
    }
}
