﻿
using FizzBuzz.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Divisibility
{
    public class DivisibleByThree : IDivisible
    {
        private readonly ICheckForDay dayToday;
        public DivisibleByThree(ICheckForDay dayToday)
        {
            this.dayToday = dayToday;
        }

        public bool IsDivisible(int Number)
        {
            return Number % 3 == 0;
        }
        public string GetMessage()
        {
            return (this.dayToday.IsWednesday(DateTime.Today.DayOfWeek)) == true ? "Wizz" : "Fizz";
        }
    }
}
