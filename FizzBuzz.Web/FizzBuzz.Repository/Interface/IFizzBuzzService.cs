﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Interface
{
    public interface IFizzBuzzService
    {
        IList<string> GetList(int number);
    }
}
