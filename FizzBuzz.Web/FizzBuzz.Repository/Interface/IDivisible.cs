﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Repository.Interface
{
    public interface IDivisible
    {
        bool IsDivisible(int Number);
        string GetMessage();
    }
}
